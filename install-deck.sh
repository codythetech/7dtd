#!/bin/bash

GAME_DIR='/home/deck/.local/share/Steam/steamapps/common/7 Days To Die/'

mkdir -p "$GAME_DIR"

# this will download the latest artifact from the modpack job on the main branch
curl -iL https://gitlab.com/api/v4/projects/53809804/jobs/artifacts/main/download?job=modpack > modpack.zip

unzip -d "$GAME_DIR" -o modpack.zip 
rm -f modpack.zip