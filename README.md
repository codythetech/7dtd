# 7dtd

## Getting started

To install the necessary client-side mods for our server's current modpack on your client

```
curl https://gitlab.com/codythetech/7dtd/-/raw/main/install-deck.sh | bash
```
